package com.burak.musicland

import android.app.Application
import com.burak.musicland.di.component.AppComponent
import com.burak.musicland.di.component.DaggerAppComponent
import com.burak.musicland.di.subcomponent.AlbumsViewModelSubComponent

class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        this.appComponent = this.initDagger()
    }

    fun initDagger() =
         DaggerAppComponent.create()
}