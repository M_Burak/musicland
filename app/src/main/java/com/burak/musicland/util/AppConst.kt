package com.burak.musicland.util

const val BASE_URL = "http://ws.audioscrobbler.com/"

const val YOUTUBE_BASE_URL = "https://www.googleapis.com/youtube/v3/"

const val SUBCRIBER_ON = "SubscribeOn"

const val OBSERVER_ON = "ObserverOn"

const val TIMEOUT_REQUEST = 60L

const val LIMIT = 20

const val METHOD_ALBUM_SEARCH = "album.search"

const val METHOD_ALBUM_INFO = "album.getinfo"

const val METHOD_ARTIST_SEARCH = "artist.search"

const val METHOD_TOP_ALBUMS = "artist.gettopalbums"

const val METHOD_ARTIST_INFO = "artist.getinfo"

const val METHOD_TRACK_SEARCH = "track.search"

const val METHOD_TRACK_INFO = "track.getinfo"

const val METHOD_TAG_TRACKS = "tag.gettoptracks"

const val FORMAT = "json"

const val API_KEY = "42330979822b33effb8d85608da28df5"

const val SNIPPET = "snippet"

const val YOUTUBE_API_KEY = "AIzaSyAhNZAAV5QBDLi17XenHlDhyvZMuXK-WHo"