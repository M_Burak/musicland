package com.burak.musicland.useCases

import com.burak.musicland.model.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LastFmUseCase @Inject constructor(
    private val service: LastFmService,
    private val youtubeService: YoutubeService
) {
    fun searchAlbums(album: String): Observable<Album> = service.searchAlbums(album)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .flatMapIterable {
            it.results.albummatches?.album
        }
        .filter {
            it.name?.contains(album, true)!!
        }

    fun getAlbumInfo(artist: String?, album: String?): Observable<AlbumInfoResponse>? =
        service.getAlbumInfo(artist, album)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())


    fun searchArtists(artist: String): Observable<ArtistsResponse> = service.searchArtists(artist)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun getArtistInfoWithTopAlbums(artist: String): Single<ArtistInfoResponse> {
        return Single.zip(
            service.getArtistInfo(artist),
            service.getTopAlnums(artist),
            BiFunction<ArtistInfoResponse, TopAlbumsResponse, ArtistInfoResponse>
            { artistInfoResponse: ArtistInfoResponse, topAlbumsResponse: TopAlbumsResponse ->
                artistInfoResponse.artist?.topalbums = topAlbumsResponse.topalbums.album
                artistInfoResponse.artist?.image = topAlbumsResponse.topalbums?.album[0]?.image
                return@BiFunction artistInfoResponse
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchTracks(track: String): Observable<SearchResponse> = service.searchTracks(track)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())


    fun getTracksIds(track: String): Single<YoutubeResponse> {
        return youtubeService.getVideos(track)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getTrackInfoWithVideoId(artist: String, track: String): Single<TrackInfo> {
        return Single.zip(
            service.getTrackInfo(artist,track),
            getTracksIds("$track $artist"),
            BiFunction<TrackInfo, YoutubeResponse, TrackInfo>
            { trackInfoResponse: TrackInfo, trackId: YoutubeResponse->
                trackInfoResponse.track?.mbid = trackId.items?.get(0)?.id?.videoId
                return@BiFunction trackInfoResponse
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun searchTracksByTag(tag: String): Observable<Results> = service.getTracksByTags(tag)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

}