package com.burak.musicland.viewmodel

import androidx.lifecycle.MutableLiveData
import com.burak.musicland.model.*
import com.burak.musicland.useCases.LastFmUseCase
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class TracksViewModel @Inject constructor(val useCase: LastFmUseCase) : BaseViewModel() {

    var trackSearchObserver = MutableLiveData<SearchResponse>()
    var trackSearchByTagObserver = MutableLiveData<Results>()
    var trackYoutubeIdObserver= MutableLiveData<TrackInfo>()
    var isLoading = MutableLiveData<Boolean>()
    var loadingError = MutableLiveData<Boolean>()

    fun searchForTracks(track: String) {
        isLoading.value = true
        disposable.add(
            useCase.searchTracks(track)
                .subscribeWith(object : DisposableObserver<SearchResponse>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: SearchResponse) {
                        trackSearchObserver.value = t
                        isLoading.value = false
                        loadingError.value = false

                    }

                    override fun onError(e: Throwable) {
                        isLoading.value = false
                        loadingError.value = true
                    }

                })
        )
    }

    fun getYoutubeVideoId(artist:String,track: String) {
        isLoading.value = true
        disposable.add(
            useCase.getTrackInfoWithVideoId(artist,track)
                .subscribeWith(object : DisposableSingleObserver<TrackInfo>() {
                    override fun onSuccess(t: TrackInfo) {
                        trackYoutubeIdObserver.value = t
                    }


                    override fun onError(e: Throwable) {
                        isLoading.value = false
                        loadingError.value = true
                    }

                })
        )
    }

    fun searchForTracksByTag(tag: String) {
        isLoading.value = true
        disposable.add(
            useCase.searchTracksByTag(tag)
                .subscribeWith(object : DisposableObserver<Results>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: Results) {
                        trackSearchByTagObserver.value = t
                        isLoading.value = false
                        loadingError.value = false

                    }

                    override fun onError(e: Throwable) {
                        isLoading.value = false
                        loadingError.value = true
                    }

                })
        )
    }
}