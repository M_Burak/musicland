package com.burak.musicland.viewmodel

import androidx.lifecycle.MutableLiveData
import com.burak.musicland.model.Album
import com.burak.musicland.useCases.LastFmUseCase
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class AlbumsViewModel @Inject constructor(val useCase: LastFmUseCase) : BaseViewModel() {

    var albums = MutableLiveData<MutableList<Album>>()
    var isLoading = MutableLiveData<Boolean>()
    var loadingError = MutableLiveData<Boolean>()
    var list = mutableListOf<Album>()

    fun getAlbums(albumSearch: String) {
        isLoading.value = true
        list.clear()
        disposable.add(
            useCase.searchAlbums(albumSearch).subscribeWith(object :
                DisposableObserver<Album>() {
                override fun onComplete() {
                    albums.value = list
                    isLoading.value = false
                    loadingError.value = false
                }

                override fun onNext(t: Album) {
                    list?.add(t)
                }

                override fun onError(e: Throwable) {
                    isLoading.value = false
                    loadingError.value = true
                }
            })
        )
    }
}