package com.burak.musicland.viewmodel

import androidx.lifecycle.MutableLiveData
import com.burak.musicland.model.Album
import com.burak.musicland.model.AlbumInfoResponse
import com.burak.musicland.model.ArtistInfoResponse
import com.burak.musicland.model.ArtistsResponse
import com.burak.musicland.useCases.LastFmUseCase
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class ArtistsViewModel @Inject constructor(val useCase: LastFmUseCase) : BaseViewModel() {
    var artist = MutableLiveData<ArtistsResponse>()
    var artistInfoResponse = MutableLiveData<ArtistInfoResponse>()
    var isLoading = MutableLiveData<Boolean>()
    var loadingError = MutableLiveData<Boolean>()
    val albume = MutableLiveData<Album>()

    fun getArtists(artistSearch: String) {
        isLoading.value = true
        disposable.add(
            useCase.searchArtists(artistSearch).subscribeWith(object :
                DisposableObserver<ArtistsResponse>() {
                override fun onComplete() {

                }

                override fun onNext(t: ArtistsResponse) {
                    artist.value = t
                    isLoading.value = false
                    loadingError.value = false
                }

                override fun onError(e: Throwable) {
                    isLoading.value = false
                    loadingError.value = true
                }
            })
        )
    }

    fun getArtistInfo(artist:String){
        isLoading.value = true
        disposable.add(
            useCase.getArtistInfoWithTopAlbums(artist).subscribeWith(object: DisposableSingleObserver<ArtistInfoResponse>(){
                override fun onSuccess(t: ArtistInfoResponse) {
                    artistInfoResponse.value = t
                    isLoading.value = false
                    loadingError.value = false
                }

                override fun onError(e: Throwable) {
                    isLoading.value = false
                    loadingError.value = true
                }


            })
        )
    }

    fun getAlbumInfo(artist: String?, album: String?) {
        isLoading.value = true
        useCase.getAlbumInfo(artist, album)?.subscribeWith(object :
            DisposableObserver<AlbumInfoResponse>() {
            override fun onComplete() {

            }

            override fun onNext(t: AlbumInfoResponse) {
                albume.value = t.album
                loadingError.value = false
                isLoading.value = false
            }

            override fun onError(e: Throwable) {
                loadingError.value = true
                isLoading.value = false
            }


        })?.let {
            disposable.add(
                it
            )
        }

    }
}