package com.burak.musicland.viewmodel

import androidx.lifecycle.MutableLiveData
import com.burak.musicland.model.Album
import com.burak.musicland.model.AlbumInfoResponse
import com.burak.musicland.model.TrackInfo
import com.burak.musicland.useCases.LastFmUseCase
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class AlbumDetailViewModel @Inject constructor(val useCase: LastFmUseCase):BaseViewModel() {
    val albume = MutableLiveData<Album>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()
    var trackYoutubeIdObserver= MutableLiveData<TrackInfo>()

    fun getAlbumInfo(artist: String?, album: String?) {
        loading.value = true
        useCase.getAlbumInfo(artist, album)?.subscribeWith(object :
            DisposableObserver<AlbumInfoResponse>() {
            override fun onComplete() {

            }

            override fun onNext(t: AlbumInfoResponse) {
                albume.value = t.album
                loadError.value = false
                loading.value = false
            }

            override fun onError(e: Throwable) {
                loadError.value = true
                loading.value = false
            }


        })?.let {
            disposable.add(
                it
            )
        }

    }

    fun getYoutubeVideoId(artist:String,track: String) {
        loading.value = true
        disposable.add(
            useCase.getTrackInfoWithVideoId(artist,track)
                .subscribeWith(object : DisposableSingleObserver<TrackInfo>() {
                    override fun onSuccess(t: TrackInfo) {
                        trackYoutubeIdObserver.value = t
                    }


                    override fun onError(e: Throwable) {
                        loading.value = false
                        loadError.value = true
                    }

                })
        )
    }
}