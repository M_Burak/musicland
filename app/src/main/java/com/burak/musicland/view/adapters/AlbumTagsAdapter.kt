package com.burak.musicland.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.burak.musicland.R
import com.burak.musicland.model.Tag
import kotlinx.android.synthetic.main.item_tag.view.*

class AlbumTagsAdapter (var tags : ArrayList<Tag>) : RecyclerView.Adapter<AlbumTagsAdapter.AlbumTagsViewHolder>() {

    fun updateTagList(newTags: List<Tag>){
        tags.clear()
        tags.addAll(newTags)
        notifyDataSetChanged()
    }

    override fun getItemCount() = tags.size

    override fun onBindViewHolder(holder: AlbumTagsViewHolder, position: Int) {
        tags[position]?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumTagsViewHolder {
        return  AlbumTagsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_tag,parent,false)
        )
    }

    class AlbumTagsViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val tagName = view.tagName

        fun bind(tag: Tag){
            tagName.text = tag.name
        }
    }
}