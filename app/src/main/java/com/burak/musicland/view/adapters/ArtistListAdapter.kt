package com.burak.musicland.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.burak.musicland.R
import com.burak.musicland.model.Artist
import com.burak.musicland.util.getProgressDrawable
import com.burak.musicland.util.loadImage
import kotlinx.android.synthetic.main.item_artist.view.*


class ArtistListAdapter(val artists : ArrayList<Artist>): RecyclerView.Adapter<ArtistListAdapter.ArtistsListHolder>() {
    fun updateArtists(newArtists : List<Artist>){
        artists.clear()
        artists.addAll(newArtists)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ArtistsListHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_artist,parent,false)
    )

    override fun getItemCount() = artists.size

    override fun onBindViewHolder(holder: ArtistsListHolder, position: Int) {
        holder.bind(artists[position])
    }


    class ArtistsListHolder(view: View):RecyclerView.ViewHolder(view)
    {
        private val artistName = view.artistName
        private val artistPhoto = view.ivArtist

        private val progressDrawable = getProgressDrawable(view.context)

        fun bind(artist: Artist){
            artistName.text = artist.name
           // artistPhoto.loadImage(artist.image?.get(3)?.text,progressDrawable)
            artistPhoto.loadImage("https://www.last.fm/music/Justin+Timberlake/+images/3a9ea4907b998f7d1d56a802299bb0e6",progressDrawable)
        }
    }
}