package com.burak.musicland.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.burak.musicland.R
import kotlinx.android.synthetic.main.item_general_string.view.*
import kotlinx.android.synthetic.main.item_tag.view.*

class GeneralStringAdapter(var tags: ArrayList<String>,var listener : GeneralStringAdapterListener, var type:String) :
    RecyclerView.Adapter<GeneralStringAdapter.StringViewHolder>() {

    fun updateData(newStringList: MutableList<String>){
        tags.clear()
        tags.addAll(newStringList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder {
        return StringViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_general_string,parent,false)
        )
    }

    override fun getItemCount() = tags.size

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        holder.bind(tags[position],position)

        holder.ivNext.setOnClickListener{
            listener.onItemClicked(tags[position],type,position)
        }
    }


    class StringViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var strGeneralTag = view.name
        var tvGeneralSira= view.tvSira
        var ivNext = view.ivNext
        fun bind(str: String, position: Int) {
            strGeneralTag.text = str
            tvGeneralSira.text = (position+1).toString()
        }
    }

    interface GeneralStringAdapterListener{
         fun onItemClicked(albumName: String,type: String,position: Int)
    }
}