package com.burak.musicland.view


import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.burak.musicland.App

import com.burak.musicland.R
import com.burak.musicland.model.Album
import com.burak.musicland.model.Tag
import com.burak.musicland.model.TrackInfo
import com.burak.musicland.util.getProgressDrawable
import com.burak.musicland.util.loadImage
import com.burak.musicland.view.adapters.AlbumTagsAdapter
import com.burak.musicland.view.adapters.AlbumTracksAdapter
import com.burak.musicland.viewmodel.AlbumDetailViewModel
import com.burak.musicland.viewmodel.DaggerViewModelFactory
import kotlinx.android.synthetic.main.fragment_album_detail.*
import kotlinx.android.synthetic.main.fragment_album_detail.rwTracks
import java.text.DecimalFormat
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class AlbumDetailFragment : BaseFragment(),AlbumTracksAdapter.AlbumTracksClickedListener {
    override fun onAlbumTrackLastFmClicked(url: String) {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(url)
        startActivity(openURL)
    }

    override fun onAlbumTrackClicked(artist: String, track: String) {
        detailViewModel.getYoutubeVideoId(artist, track)
    }

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    private lateinit var detailViewModel: AlbumDetailViewModel
    private var albumInfo: Album? = null



    private val trackListAdapter = AlbumTracksAdapter(arrayListOf(),this)
    private val tagListAdapter = AlbumTagsAdapter(arrayListOf())


    private val albumListDataObserver = Observer<Album> { it ->
        it?.let { it ->
            it.playcount?.let {
                var plays= it.toDouble()
                val df = DecimalFormat("#,###.##")
                val finalCount = df.format(plays).toString()
                tvPlaycount.text = getString(R.string.playCountString)+" "+ finalCount
            }
            tvSinger.text = it.artist
            tvAlbumName.text = it.name
            it.tracks?.track?.let { tracks->
                trackListAdapter.updateTrackList(tracks)
            }

            it.tags?.tag?.let {tags->
                var tagsList = mutableListOf<Tag>()
                tagsList.addAll(tags)
                for ( i in 0 until tagsList.size){
                    if (tagsList[i].name.equals(getString(R.string.ownAlbums),true))
                    {
                        tagsList.removeAt(i)
                        break
                    }
                }
                tagListAdapter.updateTagList(tagsList)
            }
            val progressDrawable = getProgressDrawable(ivArtist.context)
            ivArtist.loadImage(it.image?.get(3)?.text, progressDrawable)
            it?.image?.get(3)?.text?.let { it1 -> setupBGColor(it1) }
        }
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        pBarDetail.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private var ytObserver = Observer<TrackInfo> {
        val i = Intent(context, YoutubePLayerActivity::class.java)
        i.putExtra(getString(R.string.id),it.track?.mbid)
        if (it.track?.wiki?.content != null)
            i.putExtra(getString(R.string.info), it.track?.wiki?.content?.split("<a")?.get(0))
        else{
            i.putExtra(getString(R.string.info),"")
        }
        i.putExtra(getString(R.string.img), it.track?.album?.image?.get(3)?.text)
        pBarDetail.visibility = View.GONE
        startActivity(i)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_album_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            albumInfo = AlbumDetailFragmentArgs.fromBundle(it).album
        }

        rwTracks.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = trackListAdapter
        }

        rwTags.apply {
            layoutManager = GridLayoutManager(context,3)
            adapter = tagListAdapter
        }

        (activity?.applicationContext as App).appComponent
            .newAlbumDetailComponent().inject(this)
        detailViewModel = ViewModelProviders.of(this, viewModelFactory)[AlbumDetailViewModel::class.java]

        observeAlbumDetail()
        detailViewModel.getAlbumInfo(albumInfo?.name, albumInfo?.artist)
    }

    private fun observeAlbumDetail() {
        detailViewModel.albume.observe(this, albumListDataObserver)
        detailViewModel.loading.observe(this, loadingLiveDataObserver)
        detailViewModel.trackYoutubeIdObserver.observe(this, ytObserver)
    }

    private fun setupBGColor(url: String) {
        Glide.with(this)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {

                }

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    Palette.from(resource)
                        .generate() { palette ->
                            val intColor =
                                palette?.lightMutedSwatch?.rgb ?: palette?.lightVibrantSwatch?.rgb
                                ?: 0
                            llDetails.setBackgroundColor(intColor)
                        }
                }

            })
    }

    override fun getLayoutById() = R.layout.fragment_album_detail


}
