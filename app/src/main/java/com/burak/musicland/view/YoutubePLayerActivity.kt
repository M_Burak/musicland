package com.burak.musicland.view

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.palette.graphics.Palette
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.burak.musicland.R
import com.burak.musicland.util.YOUTUBE_API_KEY
import com.burak.musicland.util.getProgressDrawable
import com.burak.musicland.util.loadImage
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import kotlinx.android.synthetic.main.activity_youtube_player.*
import kotlinx.android.synthetic.main.fragment_album_detail.*

class YoutubePLayerActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {

    var videoId= ""
    var info = ""
    override fun onInitializationSuccess(
        p0: YouTubePlayer.Provider?,
        p1: YouTubePlayer?,
        p2: Boolean
    ) {
            p1?.cueVideo(videoId)
            p1?.play()
      }

    override fun onInitializationFailure(
        p0: YouTubePlayer.Provider?,
        p1: YouTubeInitializationResult?
    ) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_youtube_player)
        videoId = intent.getStringExtra(getString(R.string.id))
        info = intent.getStringExtra(getString(R.string.info))
        youtubePlayerView.initialize(YOUTUBE_API_KEY, this)
        info.let {
            if (it != "")
            {
                tvInfo.text = info
            }else
            {
                tvInfo.visibility = View.GONE
                ivAlbumImage.visibility = View.VISIBLE

            }
            val progressDrawable = getProgressDrawable(this)
            ivAlbumImage.loadImage(intent.getStringExtra(getString(R.string.img)),progressDrawable)
            setupBGColor(intent.getStringExtra(getString(R.string.img)))
        }


    }

    private fun setupBGColor(url: String) {
        Glide.with(this)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {

                }

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    Palette.from(resource)
                        .generate() { palette ->
                            val intColor =
                                palette?.lightMutedSwatch?.rgb ?: palette?.lightVibrantSwatch?.rgb
                                ?: 0
                            rwYoutube.setBackgroundColor(intColor)
                        }
                }

            })
    }
}
