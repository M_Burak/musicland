package com.burak.musicland.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.burak.musicland.App

import com.burak.musicland.R
import com.burak.musicland.model.Album
import com.burak.musicland.view.adapters.AlbumListAdapter
import com.burak.musicland.viewmodel.AlbumsViewModel
import com.burak.musicland.viewmodel.DaggerViewModelFactory
import kotlinx.android.synthetic.main.fragment_album.*
import kotlinx.android.synthetic.main.top_bar.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class AlbumFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    private lateinit var albumsViewModel: AlbumsViewModel

    private val listAdapter = AlbumListAdapter(arrayListOf())

    private val albumListDataObserver = Observer<List<Album>> { list ->
        list?.let {
            rwAlbumList.visibility = View.VISIBLE
            listAdapter.updateAlbumList(it)
        }
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        pBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            tvError.visibility = View.GONE
            rwAlbumList.visibility = View.GONE
        }

    }

    private val loadErrorLiveDataObserver = Observer<Boolean> { loadingError ->
        tvError.visibility = if (loadingError) View.VISIBLE else View.GONE
    }

    override fun getLayoutById() = R.layout.fragment_album

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appTitle.visibility = View.GONE
        ivSearch.visibility = View.VISIBLE
        searchView.visibility = View.VISIBLE
        searchView.hint = getString(R.string.svAlbums)

        (activity?.applicationContext as App).appComponent.newAlbumsComponent().inject(this)
        albumsViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(AlbumsViewModel::class.java)

        rwAlbumList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }

        ivSearch.setOnClickListener{
            albumsViewModel.getAlbums(searchView.text.toString())
        }

        observeAlbumList()
        albumsViewModel.getAlbums("Believe")
    }


    private  fun observeAlbumList() {
        albumsViewModel.albums.observe(this, albumListDataObserver)
        albumsViewModel.isLoading.observe(this, loadingLiveDataObserver)
        albumsViewModel.loadingError.observe(this, loadErrorLiveDataObserver)
    }
}
