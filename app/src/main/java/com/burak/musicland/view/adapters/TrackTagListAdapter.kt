package com.burak.musicland.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.burak.musicland.R
import com.burak.musicland.model.Album
import com.burak.musicland.model.TrackSearch
import com.burak.musicland.model.TrackSearchTag
import com.burak.musicland.util.getProgressDrawable
import com.burak.musicland.util.loadImage
import com.burak.musicland.view.AlbumFragmentDirections
import kotlinx.android.synthetic.main.item_album.view.*
import kotlinx.android.synthetic.main.item_track.view.*

class TrackTagListAdapter(var tracks: ArrayList<TrackSearchTag>, val listener:TrackItemClickedListener) :
    RecyclerView.Adapter<TrackTagListAdapter.TrackViewHolder>() {
    fun updateTracksList(newTracks: List<TrackSearchTag>?) {
        tracks.clear()
        if (newTracks != null) {
            tracks.addAll(newTracks)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TrackViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_track, parent, false)
        )

    override fun getItemCount() = tracks.size

    override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
        tracks.get(position).let { holder.bind(it) }

        holder.ytImage.setOnClickListener{
            listener.onTrackClicked(tracks[position].artist?.name,tracks[position].name)
        }

        holder.lastFmImage.setOnClickListener{
            tracks[position].url?.let { it1 -> listener.onTrackUrlClicked(it1) }
        }
    }

    class TrackViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val trackName = view.tvSong
        private val artistName = view.tvArtist
        private val trackPhoto = view.ivTracks
        val ytImage = view.ivYt
        val lastFmImage = view.ivYtLastFm

        private val progressDrawable = getProgressDrawable(view.context)

        fun bind(track: TrackSearchTag) {
            trackName.text = track.name
            artistName.text = track.artist?.name
            trackPhoto.loadImage(track.image?.get(3)?.text, progressDrawable)
        }
    }

    interface TrackItemClickedListener {
        fun onTrackClicked(artist: String?, track:String?)
        fun onTrackUrlClicked(url:String)
    }
}