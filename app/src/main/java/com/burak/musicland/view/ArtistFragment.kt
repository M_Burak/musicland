package com.burak.musicland.view


import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.burak.musicland.App
import com.burak.musicland.util.getProgressDrawable

import com.burak.musicland.R
import com.burak.musicland.model.Album
import com.burak.musicland.model.AlbumTwo
import com.burak.musicland.model.ArtistInfoResponse
import com.burak.musicland.model.ArtistsResponse
import com.burak.musicland.util.loadImage
import com.burak.musicland.view.adapters.AlbumTagsAdapter
import com.burak.musicland.view.adapters.ArtistListAdapter
import com.burak.musicland.view.adapters.GeneralStringAdapter
import com.burak.musicland.viewmodel.AlbumDetailViewModel
import com.burak.musicland.viewmodel.ArtistsViewModel
import com.burak.musicland.viewmodel.DaggerViewModelFactory
import kotlinx.android.synthetic.main.fragment_album_detail.*
import kotlinx.android.synthetic.main.fragment_artist.*
import kotlinx.android.synthetic.main.fragment_artist.ivArtist
import kotlinx.android.synthetic.main.fragment_artist.pBar
import kotlinx.android.synthetic.main.fragment_artist.tvSinger
import kotlinx.android.synthetic.main.fragment_entrance.*
import kotlinx.android.synthetic.main.top_bar.*
import java.text.FieldPosition
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ArtistFragment : BaseFragment(),GeneralStringAdapter.GeneralStringAdapterListener {
    private var artistInfoResponse = ArtistInfoResponse(null)

    override fun onItemClicked(albumName: String, type: String, position: Int) {
        if(type == "artist")
        artistsViewModel.getArtistInfo(albumName)
        else{
            var album = Album(null, artistInfoResponse.artist?.name,null,
                artistInfoResponse.artist?.topalbums?.get(position)?.name,"","","","",null
            )

                val action = ArtistFragmentDirections.actionArtistFragmentToAlbumDetailFragment(album)
                Navigation.findNavController(rwTopAlbums).navigate(action)

        }
    }

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    private lateinit var artistsViewModel: ArtistsViewModel
   // private lateinit var albumsViewModel: AlbumDetailViewModel

    private val listAdapter = GeneralStringAdapter(arrayListOf(),this,"artist")
    private val listAlbumAdapter = GeneralStringAdapter(arrayListOf(),this,"album")

    private val artistInfoResponseDataObserver = Observer<ArtistInfoResponse>
    {
        artistInfoResponse.artist = it.artist
        tvBio.text = it.artist?.bio?.summary?.split("<a")!![0]
        tvSinger.text = it.artist?.name
        rwTopAlbums.visibility = View.VISIBLE
        it.artist?.similar?.let { similar->

            var strList = mutableListOf<String>()

            similar.artist?.let {artist->
                if(artist.size<5)
                artist.forEach{artistX->
                    strList.add(artistX.name)
                }else
                    for (y in 0 until 5){
                        strList.add(artist[y].name)
                    }
                listAdapter.updateData(strList)
            }
        }

        it.artist?.topalbums?.let { topAlbums->

            var strList2 = mutableListOf<String>()

            if(topAlbums.size<5)
                topAlbums.forEach{albumTwo->
                    strList2.add(albumTwo.name)
                }else
                for (i in 0 until 5){
                    strList2.add(topAlbums[i].name)
                }
            listAlbumAdapter.updateData(strList2)
        }


        val progressDrawable = getProgressDrawable(ivArtist.context)
        ivArtist.loadImage(it.artist?.image?.get(3)?.text, progressDrawable)
        it?.artist?.image?.get(3)?.text?.let { it1 -> setupBGColor(it1) }

        tvSimilar.text = getString(R.string.similar)
        tvTopAlbums.text = getString(R.string.topAlbums)
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->
        pBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            //   tvError.visibility = View.GONE
            rwTopAlbums.visibility = View.GONE
        }

    }

    private val loadErrorLiveDataObserver = Observer<Boolean> { loadingError ->
        // tvError.visibility = if (loadingError) View.VISIBLE else View.GONE
    }

    override fun getLayoutById() = R.layout.fragment_album

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_artist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appTitle.visibility = View.GONE
        ivSearch.visibility = View.VISIBLE
        searchView.visibility = View.VISIBLE
        searchView.hint = getString(R.string.svArtists)

        (activity?.applicationContext as App).appComponent.newArtistsComponent().inject(this)
        artistsViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(ArtistsViewModel::class.java)

    //    albumsViewModel= ViewModelProviders.of(this, viewModelFactory).get(AlbumDetailViewModel::class.java)

        rwSimilarArtists.apply {
            layoutManager = GridLayoutManager(context,1)
            adapter = listAdapter
        }

        rwTopAlbums.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAlbumAdapter
        }

        ivSearch.setOnClickListener {
            artistsViewModel.getArtistInfo(searchView.text.toString())
        }

        observeAlbumList()
        artistsViewModel.getArtistInfo("Inna")
    }


    private fun observeAlbumList() {
        artistsViewModel.isLoading.observe(this, loadingLiveDataObserver)
        artistsViewModel.loadingError.observe(this, loadErrorLiveDataObserver)
        artistsViewModel.artistInfoResponse.observe(this,artistInfoResponseDataObserver)
    }


    private fun setupBGColor(url: String) {
        Glide.with(this)
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {

                }

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    Palette.from(resource)
                        .generate() { palette ->
                            val intColor =
                                palette?.lightMutedSwatch?.rgb ?: palette?.lightVibrantSwatch?.rgb ?: palette?.mutedSwatch?.rgb
                                ?: 0
                            swArtists.setBackgroundColor(intColor)
                        }
                }

            })
    }

}
