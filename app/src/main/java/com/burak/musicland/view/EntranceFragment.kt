package com.burak.musicland.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation

import com.burak.musicland.R
import kotlinx.android.synthetic.main.fragment_entrance.*

/**
 * A simple [Fragment] subclass.
 */
class EntranceFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_entrance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvAlbum.setOnClickListener{
            goToAlbums()
        }

        tvArtist.setOnClickListener{
            goToArtists()
        }

        tvTrack.setOnClickListener{
            goToTracks()
        }

        tvGenre.setOnClickListener{
            goToTags()
        }
    }

    fun goToAlbums(){
        val action = EntranceFragmentDirections.actionEntranceFragmentToAlbumFragment()
        Navigation.findNavController(tvAlbum).navigate(action)

    }

    fun goToArtists(){
        val action = EntranceFragmentDirections.actionEntranceFragmentToArtistFragment()
        Navigation.findNavController(tvArtist).navigate(action)
    }

    fun goToTracks(){
        val action = EntranceFragmentDirections.actionEntranceFragmentToTrackFragment()
        Navigation.findNavController(tvTrack).navigate(action)
    }

    fun goToTags(){
        val action = EntranceFragmentDirections.actionEntranceFragmentToTagFragment()
        Navigation.findNavController(tvGenre).navigate(action)
    }
}
