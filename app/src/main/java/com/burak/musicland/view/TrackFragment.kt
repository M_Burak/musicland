package com.burak.musicland.view


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.burak.musicland.App

import com.burak.musicland.R
import com.burak.musicland.model.SearchResponse
import com.burak.musicland.model.TrackInfo

import com.burak.musicland.view.adapters.TrackListAdapter

import com.burak.musicland.viewmodel.DaggerViewModelFactory
import com.burak.musicland.viewmodel.TracksViewModel
import kotlinx.android.synthetic.main.fragment_track.*
import kotlinx.android.synthetic.main.item_track.*
import kotlinx.android.synthetic.main.top_bar.*

import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class TrackFragment : BaseFragment(),TrackListAdapter.TrackItemClickedListener {
    override fun onTrackUrlClicked(url: String) {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(url)
        startActivity(openURL)
    }

    override fun onTrackClicked(artist: String?,track:String?) {
        artist?.let { track?.let { it1 -> tracksViewModel.getYoutubeVideoId(it, it1) } }
    }

    val tracksAdapter = TrackListAdapter(arrayListOf(),this)

    override fun getLayoutById(): Int {
        return R.layout.fragment_track
    }

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory
    private lateinit var tracksViewModel: TracksViewModel

    private var ytObserver = Observer<TrackInfo> {
        val i = Intent(context, YoutubePLayerActivity::class.java)
        i.putExtra(getString(R.string.id),it.track?.mbid)
        if (it.track?.wiki?.content != null)
        i.putExtra(getString(R.string.info), it.track?.wiki?.content?.split("<a")?.get(0))
        else{
            i.putExtra(getString(R.string.info),"")
        }
        i.putExtra(getString(R.string.img), it.track?.album?.image?.get(3)?.text)
        pBar.visibility = View.GONE
        startActivity(i)
    }

    private var tracksObserver = Observer<SearchResponse> {
        it?.results?.trackmatches?.track?.let { tracks ->
            tracksAdapter.updateTracksList(tracks)
            pBar.visibility = View.GONE
            tvError.visibility = View.GONE
        }
    }

    private var errorObserver = Observer<Boolean> {

        if (it) {
            pBar.visibility = View.GONE
            tvError.visibility = View.VISIBLE
        } else {
            pBar.visibility = View.GONE
            tvError.visibility = View.GONE
        }
    }

    private var loadingObserver = Observer<Boolean> {

        if (it) {
            pBar.visibility = View.VISIBLE
            tvError.visibility = View.GONE
        } else {
            pBar.visibility = View.GONE
            tvError.visibility = View.GONE
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_track, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rwTracks.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = tracksAdapter
        }

        appTitle.visibility = View.GONE
        ivSearch.visibility = View.VISIBLE
        searchView.visibility = View.VISIBLE
        searchView.hint = getString(R.string.svTracks)

        ivSearch.setOnClickListener{
            tracksViewModel.searchForTracks(searchView.text.toString())
        }

        (activity?.applicationContext as App).appComponent.newTracksSubcomponent().inject(this)
        tracksViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(TracksViewModel::class.java)
        observeViewModel()
        tracksViewModel.searchForTracks("love")


    }

    fun observeViewModel() {
        tracksViewModel.loadingError.observe(this, errorObserver)
        tracksViewModel.isLoading.observe(this, loadingObserver)
        tracksViewModel.trackSearchObserver.observe(this, tracksObserver)
        tracksViewModel.trackYoutubeIdObserver.observe(this, ytObserver)
    }
}
