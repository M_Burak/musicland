package com.burak.musicland.model

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "track"
)
data class TrackInfo(
    @JsonProperty("track")
    val track: Track?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Track(
    @JsonProperty("album")
    val album: Album?,
    @JsonProperty("artist")
    val artist: Artist?,
    @JsonProperty("duration")
    val duration: String?,
    @JsonProperty("listeners")
    val listeners: String?,
    @JsonProperty("mbid")
    var mbid: String?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("playcount")
    val playcount: String?,
    @JsonProperty("streamable")
    val streamable: Streamable?,
    @JsonProperty("toptags")
    val toptags: Toptags?,
    @JsonProperty("url")
    val url: String?,
    @JsonProperty("wiki")
    val wiki: Wiki?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Album::class.java.classLoader),
        parcel.readParcelable(Artist::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("streamable"),
        TODO("toptags"),
        parcel.readString(),
        TODO("wiki")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(album, flags)
        parcel.writeParcelable(artist, flags)
        parcel.writeString(duration)
        parcel.writeString(listeners)
        parcel.writeString(mbid)
        parcel.writeString(name)
        parcel.writeString(playcount)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Track> {
        override fun createFromParcel(parcel: Parcel): Track {
            return Track(parcel)
        }

        override fun newArray(size: Int): Array<Track?> {
            return arrayOfNulls(size)
        }
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(
    "#text",
    "fulltrack"
)
data class Streamable(
    @JsonProperty("#text")
    val text: String?,
    @JsonProperty("fulltrack")
    val fulltrack: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Wiki(
    @JsonProperty("content")
    val content: String?,
    @JsonProperty("published")
    val published: String?,
    @JsonProperty("summary")
    val summary: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class TrackSearch(
    @JsonProperty("album")
    val album: Album?,
    @JsonProperty("artist")
    val artist: String?,
    @JsonProperty("duration")
    val duration: String?,
    @JsonProperty("listeners")
    val listeners: String?,
    @JsonProperty("mbid")
    val mbid: String?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("playcount")
    val playcount: String?,
    @JsonProperty("streamable")
    val streamable: String?,
    @JsonProperty("toptags")
    val toptags: Toptags?,
    @JsonProperty("url")
    val url: String?,
    @JsonProperty("wiki")
    val wiki: Wiki?,
    @JsonProperty("image")
    val image: List<Image>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Album::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("toptags"),
        parcel.readString(),
        TODO("wiki"),
        TODO("image")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(album, flags)
        parcel.writeString(artist)
        parcel.writeString(duration)
        parcel.writeString(listeners)
        parcel.writeString(mbid)
        parcel.writeString(name)
        parcel.writeString(playcount)
        parcel.writeString(streamable)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TrackSearch> {
        override fun createFromParcel(parcel: Parcel): TrackSearch {
            return TrackSearch(parcel)
        }

        override fun newArray(size: Int): Array<TrackSearch?> {
            return arrayOfNulls(size)
        }
    }
}


@JsonIgnoreProperties(ignoreUnknown = true)
data class TrackSearchTag(
    @JsonProperty("album")
    val album: Album?,
    @JsonProperty("artist")
    val artist: Artist?,
    @JsonProperty("duration")
    val duration: String?,
    @JsonProperty("listeners")
    val listeners: String?,
    @JsonProperty("mbid")
    val mbid: String?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("playcount")
    val playcount: String?,
    @JsonProperty("streamable")
    val streamable: Streamable?,
    @JsonProperty("toptags")
    val toptags: Toptags?,
    @JsonProperty("url")
    val url: String?,
    @JsonProperty("wiki")
    val wiki: Wiki?,
    @JsonProperty("image")
    val image: List<Image>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Album::class.java.classLoader),
        parcel.readParcelable(Artist::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("streamable"),
        TODO("toptags"),
        parcel.readString(),
        TODO("wiki"),
        TODO("image")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(album, flags)
        parcel.writeParcelable(artist, flags)
        parcel.writeString(duration)
        parcel.writeString(listeners)
        parcel.writeString(mbid)
        parcel.writeString(name)
        parcel.writeString(playcount)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TrackSearchTag> {
        override fun createFromParcel(parcel: Parcel): TrackSearchTag {
            return TrackSearchTag(parcel)
        }

        override fun newArray(size: Int): Array<TrackSearchTag?> {
            return arrayOfNulls(size)
        }
    }
}