package com.burak.musicland.model

import com.burak.musicland.util.*
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class LastFmService  @Inject constructor(val api: LastFmApi) {

    fun searchAlbums(album: String): Observable<SearchResponse> {
        return api.searchAlbums(
            METHOD_ALBUM_SEARCH, album, API_KEY, FORMAT
        )
    }


    fun getAlbumInfo(artist:String?,album: String?): Observable<AlbumInfoResponse> {
        return api.getAlbumInfo(
            METHOD_ALBUM_INFO, album, artist, API_KEY, FORMAT
        )
    }

    fun searchArtists(artist: String): Observable<ArtistsResponse> {
        return api.searchArtists(
            METHOD_ARTIST_SEARCH, artist, API_KEY, FORMAT
        )
    }

    fun getArtistInfo(artist:String): Single<ArtistInfoResponse> {
        return api.getArtistInfo(
            METHOD_ARTIST_INFO, artist, API_KEY, FORMAT
        )
    }

    fun getTopAlnums(artist: String) : Single<TopAlbumsResponse> {
        return  api.getTopAlbums(METHOD_TOP_ALBUMS,artist, API_KEY, FORMAT )
    }

    fun searchTracks(track: String): Observable<SearchResponse> {
        return api.searchTracks(
            METHOD_TRACK_SEARCH, track, API_KEY, FORMAT
        )
    }

    fun getTrackInfo(artist:String,track: String): Single<TrackInfo> {
        return api.getTrackInfo(
            METHOD_TRACK_INFO, artist, track, API_KEY, FORMAT
        )
    }

    fun getTracksByTags(tag:String): Observable<Results>{
        return api.searchTracksByTag(METHOD_TAG_TRACKS,tag,API_KEY, FORMAT)
    }
}