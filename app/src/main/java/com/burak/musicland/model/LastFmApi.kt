package com.burak.musicland.model

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface LastFmApi {


    @GET("2.0/")
    fun searchAlbums(@Query("method") method: String, @Query("album") album: String, @Query("api_key") key: String
                     , @Query("format") format: String): Observable<SearchResponse>


    @GET("2.0/")
    fun getAlbumInfo(@Query("method") method: String,@Query("artist") artist: String?, @Query("album") album: String?, @Query("api_key") key: String
                     , @Query("format") format: String): Observable<AlbumInfoResponse>
    @GET("2.0/")
    fun searchArtists(@Query("method") method: String, @Query("artist") artist: String, @Query("api_key") key: String
                     , @Query("format") format: String): Observable<ArtistsResponse>




    @GET("2.0/")
    fun getTopAlbums(@Query("method") method: String, @Query("artist") artist: String, @Query("api_key") key: String
                     , @Query("format") format: String): Single<TopAlbumsResponse>


    @GET("2.0/")
    fun getArtistInfo(@Query("method") method: String,@Query("artist") artist: String?, @Query("api_key") key: String
                      , @Query("format") format: String): Single<ArtistInfoResponse>

    @GET("2.0/")
    fun searchTracks(@Query("method") method: String, @Query("track") track: String, @Query("api_key") key: String
                     , @Query("format") format: String): Observable<SearchResponse>

    @GET("2.0/")
    fun getTrackInfo(@Query("method") method: String,@Query("artist") artist: String?, @Query("track") track: String?, @Query("api_key") key: String
                     , @Query("format") format: String): Single<TrackInfo>

    @GET("2.0/")
    fun searchTracksByTag(@Query("method") method: String, @Query("tag") tag: String, @Query("api_key") key: String
                     , @Query("format") format: String): Observable<Results>
}