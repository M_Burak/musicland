package com.burak.musicland.model

import com.burak.musicland.util.SNIPPET
import com.burak.musicland.util.YOUTUBE_API_KEY
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class YoutubeService @Inject constructor(val api:YoutubeApi) {
    fun getVideos(song:String) : Single<YoutubeResponse> {
        return api.getVideo(SNIPPET, song , YOUTUBE_API_KEY)
    }
}