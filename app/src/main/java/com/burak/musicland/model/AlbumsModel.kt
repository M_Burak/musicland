package com.burak.musicland.model

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.annotation.JsonProperty


@JsonPropertyOrder(
    "#text",
    "size"
)
data class Image(
    @JsonProperty("#text")
    val text: String,
    @JsonProperty("size")
    val size: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(
    "@attr",
    "albummatches",
    "artistmatches",
    "trackmatches",
    "opensearch:Query",
    "opensearch:itemsPerPage",
    "opensearch:startIndex",
    "opensearch:totalResults"
)
data class Results(
    @JsonProperty("@attr")
    val attr: Attr?,
    @JsonProperty("albummatches")
    val albummatches: Albummatches?,
    @JsonProperty("artistmatches")
    val artistmatches: Artistmatches?,

    @JsonProperty("trackmatches")
    val trackmatches: Trackmatches?,
    @JsonProperty("tracks")
    val tracks: TrackmatchesforTag?,
    @JsonProperty("opensearch:Query")
    val opensearchQuery: OpensearchQuery?,
    @JsonProperty("opensearch:itemsPerPage")
    val opensearchItemsPerPage: String?,
    @JsonProperty("opensearch:startIndex")
    val opensearchSstartIndex: String?,
    @JsonProperty("opensearch:totalResults")
    val opensearchTotalResults: String?
)

data class Albummatches(
    @JsonProperty("album")
    val album: List<Album>
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Album(
    @JsonProperty("tags")
    val tags: Toptags?,
    @JsonProperty("artist")
    val artist: String?,
    @JsonProperty("image")
    val image: List<Image>?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("url")
    val url: String?,
    @JsonProperty("playcount")
    val playcount: String?,
    @JsonProperty("releasedate")
    val releasedate: String?,
    @JsonProperty("title")
    val title: String?,
    @JsonProperty("tracks")
    val tracks: Tracks?


) : Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("tags"),
        parcel.readString(),
        TODO("image"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("tracks")
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(artist)
        parcel.writeString(name)
        parcel.writeString(url)
        parcel.writeString(playcount)
        parcel.writeString(releasedate)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Album> {
        override fun createFromParcel(parcel: Parcel): Album {
            return Album(parcel)
        }

        override fun newArray(size: Int): Array<Album?> {
            return arrayOfNulls(size)
        }
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(
    "for",
    "position"
)
data class Attr(
    @JsonProperty("for")
    val _for: String?,
    @JsonProperty("position")
    val position: String?
)


@JsonIgnoreProperties(ignoreUnknown = true)
data class Tag(
    @JsonProperty("name")
    val name: String,
    @JsonProperty("url")
    val url: String
)


@JsonIgnoreProperties(ignoreUnknown = true)
data class Toptags(
    @JsonProperty("tag")
    val tag: List<Tag>
)


@JsonPropertyOrder(
    "#text",
    "role",
    "searchTerms",
    "startPage"
)
data class OpensearchQuery(
    @JsonProperty("#text")
    val text: String?,
    @JsonProperty("role")
    val role: String?,
    @JsonProperty("searchTerms")
    val searchTerms: String?,
    @JsonProperty("startPage")
    val startPage: String?
)

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "results"
)
data class SearchResponse(
    @JsonProperty("results")
    val results: Results
)


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "album"
)
data class AlbumInfoResponse(
    @JsonProperty("album")
    val album: Album
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Tracks(
    @JsonProperty("track")
    val track: List<Track>
)

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(
    "track",
    "tracks"
)
data class Trackmatches(
    @JsonProperty("track")
    val track: List<TrackSearch>?
)
@JsonIgnoreProperties(ignoreUnknown = true)
data class TrackmatchesforTag(
    @JsonProperty("track")
    val track: List<TrackSearchTag>?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class TopAlbumsResponse(
    @JsonProperty("topalbums")
    val topalbums: Topalbums
)
@JsonIgnoreProperties(ignoreUnknown = true)
data class Topalbums(
    @JsonProperty("album")
    val album: List<AlbumTwo>
)
