package com.burak.musicland.model

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface YoutubeApi {

    @GET("search")
    fun getVideo(@Query("part") part: String, @Query("q") song: String, @Query("key") key: String
                     ): Single<YoutubeResponse>

}