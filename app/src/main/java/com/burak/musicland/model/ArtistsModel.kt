package com.burak.musicland.model

import android.os.Parcel
import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "results"
)
data class ArtistsResponse(
    @JsonProperty("results")
    val results: Results
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Artistmatches(
    @JsonProperty("artist")
    val artist: List<Artist>?
)


@JsonIgnoreProperties(ignoreUnknown = true)
data class ArtistInfoResponse(
    @JsonProperty("artist")
    var artist: Artist?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Bio(
    @JsonProperty("content")
    val content: String?,
    @JsonProperty("links")
    val links: Links?,
    @JsonProperty("published")
    val published: String?,
    @JsonProperty("summary")
    val summary: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Links(
    @JsonProperty("link")
    val link: Link?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Link(
    @JsonProperty("#text")
    val text: String?,
    @JsonProperty("href")
    val href: String?,
    @JsonProperty("rel")
    val rel: String?
)


@JsonIgnoreProperties(ignoreUnknown = true)
data class Similar(
    @JsonProperty("artist")
    val artist: List<ArtistX>?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ArtistX(
    @JsonProperty("name")
    val name: String,
    @JsonProperty("url")
    val url: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Tags(
    @JsonProperty("tag")
    val tag: List<Tag>?
)


@JsonIgnoreProperties(ignoreUnknown = true)
data class Artist(
    @JsonProperty("mbid")
    val mbid: String?,
    @JsonProperty("name")
    val name: String?,
    @JsonProperty("url")
    val url: String?,
    @JsonProperty("image")
    var image: List<Image>?,
    @JsonProperty("listeners")
    val listeners: String?,
    @JsonProperty("streamable")
    val streamable: String?,
    @JsonProperty("bio")
    val bio: Bio?,
    @JsonProperty("ontour")
    val ontour: String?,
    @JsonProperty("similar")
    val similar: Similar?,
    @JsonProperty("stats")
    val stats: Stats?,
    @JsonProperty("tags")
    val tags: Tags?,
    @JsonProperty("topalbums")
    var topalbums: List<AlbumTwo>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("image"),
        parcel.readString(),
        parcel.readString(),
        TODO("bio"),
        parcel.readString(),
        TODO("similar"),
        TODO("stats"),
        TODO("tags"),
        parcel.createTypedArrayList(AlbumTwo)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mbid)
        parcel.writeString(name)
        parcel.writeString(url)
        parcel.writeString(listeners)
        parcel.writeString(streamable)
        parcel.writeString(ontour)
        parcel.writeTypedList(topalbums)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Artist> {
        override fun createFromParcel(parcel: Parcel): Artist {
            return Artist(parcel)
        }

        override fun newArray(size: Int): Array<Artist?> {
            return arrayOfNulls(size)
        }
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
    data class Stats(
        @JsonProperty("listeners")
        val listeners: String?,
        @JsonProperty("playcount")
        val playcount: String?
    )
    @JsonIgnoreProperties(ignoreUnknown = true)
    data class AlbumTwo(
        @JsonProperty("artist")
        val artist: Artist?,
        @JsonProperty("image")
        val image: List<Image>?,
        @JsonProperty("mbid")
        val mbid: String?,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("playcount")
        val playcount: Int?,
        @JsonProperty("@JsonProperty(\"listeners\")")
        val url: String?
    ): Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readParcelable(Artist::class.java.classLoader),
            TODO("image"),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeParcelable(artist, flags)
            parcel.writeString(mbid)
            parcel.writeString(name)
            parcel.writeValue(playcount)
            parcel.writeString(url)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<AlbumTwo> {
            override fun createFromParcel(parcel: Parcel): AlbumTwo {
                return AlbumTwo(parcel)
            }

            override fun newArray(size: Int): Array<AlbumTwo?> {
                return arrayOfNulls(size)
            }
        }
    }



