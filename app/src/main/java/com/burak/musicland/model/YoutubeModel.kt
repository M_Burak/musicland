package com.burak.musicland.model

import com.fasterxml.jackson.annotation.JsonProperty

data class YoutubeResponse(
    @JsonProperty("etag")
    val etag: String?,
    @JsonProperty("items")
    val items: List<İtem>?,
    @JsonProperty("kind")
    val kind: String?,
    @JsonProperty("nextPageToken")
    val nextPageToken: String?,
    @JsonProperty("pageInfo")
    val pageInfo: PageInfo?,
    @JsonProperty("regionCode")
    val regionCode: String?
)

data class İtem(
    @JsonProperty("etag")
    val etag: String?,
    @JsonProperty("id")
    val id: İd?,
    @JsonProperty("kind")
    val kind: String?,
    @JsonProperty("snippet")
    val snippet: Snippet?
)

data class İd(
    @JsonProperty("kind")
    val kind: String?,
    @JsonProperty("alvideoIdbum")
    val videoId: String?
)

data class Snippet(
    @JsonProperty("channelId")
    val channelId: String?,
    @JsonProperty("channelTitle")
    val channelTitle: String?,
    @JsonProperty("description")
    val description: String?,
    @JsonProperty("liveBroadcastContent")
    val liveBroadcastContent: String?,
    @JsonProperty("publishedAt")
    val publishedAt: String?,
    @JsonProperty("thumbnails")
    val thumbnails: Thumbnails?,
    @JsonProperty("title")
    val title: String?
)

data class Thumbnails(
    @JsonProperty("default")
    val default: Default?,
    @JsonProperty("high")
    val high: High?,
    @JsonProperty("medium")
    val medium: Medium?
)

data class Default(
    @JsonProperty("height")
    val height: Int?,
    @JsonProperty("url")
    val url: String?,
    @JsonProperty("width")
    val width: Int?
)

data class High(
    @JsonProperty("height")
    val height: Int?,
    @JsonProperty("url")
    val url: String?,
    @JsonProperty("width")
    val width: Int?
)

data class Medium(
    @JsonProperty("height")
    val height: Int,
    @JsonProperty("url")
    val url: String,
    @JsonProperty("width")
    val width: Int
)

data class PageInfo(
    @JsonProperty("resultsPerPage")
    val resultsPerPage: Int,
    @JsonProperty("totalResults")
    val totalResults: Int
)