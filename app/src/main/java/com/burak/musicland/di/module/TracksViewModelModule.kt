package com.burak.musicland.di.module

import androidx.lifecycle.ViewModel
import com.burak.musicland.viewmodel.AlbumsViewModel
import com.burak.musicland.viewmodel.ArtistsViewModel
import com.burak.musicland.viewmodel.TracksViewModel
import com.burak.musicland.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class TracksViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(TracksViewModel::class)
    internal abstract fun bindMyViewModel(listViewModel: TracksViewModel): ViewModel
}