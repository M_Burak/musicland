package com.burak.musicland.di.module
import com.burak.musicland.di.scope.AppScope
import com.burak.musicland.model.LastFmService
import com.burak.musicland.model.YoutubeService
import com.burak.musicland.useCases.LastFmUseCase
import dagger.Module
import dagger.Provides

@Module
class AlbumsUsecaseModule {
    @AppScope
    @Provides
    fun provideFeedUseCase(service: LastFmService,youtubeService: YoutubeService) = LastFmUseCase(service,youtubeService)
}