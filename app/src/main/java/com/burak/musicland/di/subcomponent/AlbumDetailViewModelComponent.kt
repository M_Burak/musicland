package com.burak.musicland.di.subcomponent

import com.burak.musicland.di.module.AlbumDetailViewModelModule
import com.burak.musicland.di.module.ViewModelFactoryModule
import com.burak.musicland.di.scope.FragmentScope
import com.burak.musicland.view.AlbumDetailFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [
        ViewModelFactoryModule::class,
        AlbumDetailViewModelModule::class]
)
interface AlbumDetailViewModelComponent {
    fun inject(detailFragment: AlbumDetailFragment)
}