package com.burak.musicland.di.scope

import javax.inject.Scope

@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class FragmentScope