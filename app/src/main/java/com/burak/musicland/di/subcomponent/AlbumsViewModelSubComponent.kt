package com.burak.musicland.di.subcomponent

import com.burak.musicland.di.module.AlbumsViewModelModule
import com.burak.musicland.di.module.ViewModelFactoryModule
import com.burak.musicland.di.scope.FragmentScope
import com.burak.musicland.view.AlbumFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [ViewModelFactoryModule::class,
               AlbumsViewModelModule::class]
)
interface AlbumsViewModelSubComponent {
    fun inject(albumsFragment: AlbumFragment)
}