package com.burak.musicland.di.subcomponent

import com.burak.musicland.di.module.AlbumsViewModelModule
import com.burak.musicland.di.module.ArtistViewModelModule
import com.burak.musicland.di.module.ViewModelFactoryModule
import com.burak.musicland.di.scope.FragmentScope
import com.burak.musicland.view.AlbumFragment
import com.burak.musicland.view.ArtistFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [ViewModelFactoryModule::class,
               ArtistViewModelModule::class]
)
interface ArtistsViewModelSubComponent {
    fun inject(artistsFragment: ArtistFragment)
}