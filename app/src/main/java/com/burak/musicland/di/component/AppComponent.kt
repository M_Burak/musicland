package com.burak.musicland.di.component


import com.burak.musicland.di.module.NetworkModule
import com.burak.musicland.di.scope.AppScope
import com.burak.musicland.di.subcomponent.AlbumDetailViewModelComponent
import com.burak.musicland.di.subcomponent.AlbumsViewModelSubComponent
import com.burak.musicland.di.subcomponent.ArtistsViewModelSubComponent
import com.burak.musicland.di.subcomponent.TracksViewModelSubComponent
import dagger.Component

@AppScope
@Component(
    modules = [
        NetworkModule::class
    ]
)
interface AppComponent {
    fun newAlbumsComponent(): AlbumsViewModelSubComponent
    fun newArtistsComponent(): ArtistsViewModelSubComponent
    fun newAlbumDetailComponent(): AlbumDetailViewModelComponent
    fun newTracksSubcomponent(): TracksViewModelSubComponent
}