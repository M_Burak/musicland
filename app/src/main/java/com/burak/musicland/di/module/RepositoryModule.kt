package com.burak.musicland.di.module
import com.burak.musicland.di.scope.AppScope
import com.burak.musicland.model.LastFmApi
import com.burak.musicland.model.LastFmService
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule{
    @AppScope
    @Provides
    fun provideFeedRepository(api: LastFmApi) = LastFmService(api)
}