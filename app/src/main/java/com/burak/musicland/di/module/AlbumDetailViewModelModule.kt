package com.burak.musicland.di.module

import androidx.lifecycle.ViewModel
import com.burak.musicland.viewmodel.AlbumDetailViewModel
import com.burak.musicland.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AlbumDetailViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AlbumDetailViewModel::class)
    internal abstract fun bindMyViewModel(detailViewModel: AlbumDetailViewModel): ViewModel
}