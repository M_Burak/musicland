package com.burak.musicland.di.module

import com.burak.musicland.di.scope.AppScope
import com.burak.musicland.model.LastFmApi
import com.burak.musicland.model.YoutubeApi
import com.burak.musicland.util.BASE_URL
import com.burak.musicland.util.TIMEOUT_REQUEST
import com.burak.musicland.util.YOUTUBE_BASE_URL


import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {
        @AppScope
        @Provides
        fun provideHttpLogging(): HttpLoggingInterceptor {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return loggingInterceptor
        }

        @AppScope
        @Provides
        fun provideOkhttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            return OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build()
        }


        @AppScope
        @Provides
        fun provideRetrofit(okHttpClient: OkHttpClient) =
            Retrofit.Builder().client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())


        @AppScope
        @Provides
        fun provideLastFmApi(builder: Retrofit.Builder) =
            builder.baseUrl(BASE_URL).build().create(LastFmApi::class.java)


        @AppScope
        @Provides
        fun provideYoutubeApi(builder: Retrofit.Builder) =
            builder.baseUrl(YOUTUBE_BASE_URL).build().create(YoutubeApi::class.java)
    }
