package com.burak.musicland.di.module

import androidx.lifecycle.ViewModel
import com.burak.musicland.viewmodel.AlbumsViewModel
import com.burak.musicland.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AlbumsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AlbumsViewModel::class)
    internal abstract fun bindMyViewModel(listViewModel: AlbumsViewModel): ViewModel
}