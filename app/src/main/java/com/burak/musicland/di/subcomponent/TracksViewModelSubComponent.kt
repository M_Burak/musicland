package com.burak.musicland.di.subcomponent

import com.burak.musicland.di.module.AlbumsViewModelModule
import com.burak.musicland.di.module.ArtistViewModelModule
import com.burak.musicland.di.module.TracksViewModelModule
import com.burak.musicland.di.module.ViewModelFactoryModule
import com.burak.musicland.di.scope.FragmentScope
import com.burak.musicland.view.AlbumFragment
import com.burak.musicland.view.ArtistFragment
import com.burak.musicland.view.TagFragment
import com.burak.musicland.view.TrackFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [ViewModelFactoryModule::class,
                TracksViewModelModule::class]
)
interface TracksViewModelSubComponent {
    fun inject(tracksFragment: TrackFragment)
    fun injectTagFragment(tagFragment: TagFragment)
}